# Wood U
https://bornasinovcic.github.io/wood-u/

Welcome to my wooden jewelry store mockup site! This simple and elegant website is built using HTML, CSS, and JavaScript.
## Table of Contents
- [Features](#features)
- [Installation](#installation)
- [Usage](#usage)
## Features
- **Responsive Layout:** The site is designed to provide a seamless experience across various devices, ensuring easy navigation and beautiful displays.
- **Interactive Elements:** JavaScript is used to add interactive features, enhancing the user experience.
- **Easy Checkout:** Simulate the checkout process to experience how straightforward it is to purchase our stunning wooden jewelry.
## Installation
Clone the repository to your local machine:
```bash
git clone https://gitlab.com/bornasinovcic/wood-u.git
```
Open the project in your favorite code editor to explore the code.
## Usage
1. Open `index.html` in your web browser.
2. Browse through the collection of wooden jewelry.
3. Experience the interactive elements and smooth navigation.
4. Simulate the checkout process to see how easy it is to purchase our products.